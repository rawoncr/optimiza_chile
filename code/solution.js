const express = require('express');
const router = express.Router();
const pubnub = require('../config/config');

// GET Messages listing.
router.get('/', function(req, res, next) {
    pubnub.history({
        channel: 'optimiza_chile',
        callback: function(msgs) {
            msgs[0].map(function(msg){
              msg.text = msg.text.replace(/[_+-.,!@#$%^&*();\/|<>"'∙]/g, '').trim()
            })
            console.log(JSON.stringify(msgs))
            res.status(200).json(msgs)
        },
        count: 100, // 100 is the default
        reverse: false // false is the default
    });
});

// Create new menssage
router.post('/', function(req, res, next) {
  pubnub.publish({
    channel   : 'optimiza_chile',
    message   : req.body,
    callback  : function(response) {
      res.status(201).send();
    },
    error     : function(err) {
      console.log( "ERROR!", err );
      res.status(400).json({err: err});
    }
  });
});

module.exports = router;
